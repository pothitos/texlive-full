# Fresh TeX Live Docker Image

The TeX Live distribution installed into the Fedora Docker image.

If you want to use it and work in the current directory, execute

```
docker run -it -v "$PWD":/data registry.gitlab.com/pothitos/texlive-full
```

Then, you should `cd /data` in order to access the current working directory
from inside the Docker container.
