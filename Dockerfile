FROM fedora:35

RUN dnf install -y texlive-scheme-full && \
    dnf clean all
